<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\CurlController;

class PainelController extends Controller
{
    //
    public function index(){
        $pessoas = CurlController::get("people/");
        $planetas = CurlController::get("planets/");
        $veiculos = CurlController::get("vehicles/");
        $naves = CurlController::get("starships/");
        $filmes = CurlController::get("films/");
        $especies = CurlController::get("species/");

        return view('Painel.index',[
            'quantidadePessoas' => $pessoas->count,
            'quantidadePlanetas' => $planetas->count,
            'quantidadeVeiculos' => $veiculos->count,
            'quantidadeNaves' => $naves->count,
            'quantidadeFilmes' => $filmes->count,
            'quantidadeEspecies' => $especies->count,
        ])->render();
    }
}
