<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\CurlController;

class EspeciesController extends Controller
{
    //
    public function index(){

        $especies = CurlController::get("species/");
        // dd($especies);
        return view('Painel.especies',[
            'page'=>'especies',
            'especies'=>$especies->results,
            'next'=>$especies->next,
            'previous'=>$especies->previous,
        ])->render();
    }
}
