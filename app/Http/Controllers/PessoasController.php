<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\CurlController;

class PessoasController extends Controller
{
    //
    public function index(){

        $pessoas = CurlController::get("people/");
        return view('Painel.pessoas',[
            'page'=>'pessoas',
            'pessoas'=>$pessoas->results,
            'next'=>$pessoas->next,
            'previous'=>$pessoas->previous,
        ])->render();
    }
}
