<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\CurlController;


class NavesController extends Controller
{
    //
    public function index(){
        $naves = CurlController::get("starships/");
        // dd($naves);
        return view('Painel.naves',[
            'page'=>'naves',
            'naves'=>$naves->results,
            'next'=>$naves->next,
            'previous'=>$naves->previous,
        ])->render();
    }
}
