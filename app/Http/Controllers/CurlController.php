<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CurlController extends Controller
{
    //
    public function get($url){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,"https://swapi.co/api/".$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $resp = curl_exec($curl);
        // $info = curl_getinfo($curl);
        curl_close ($ch);
        return json_decode($resp);
    }
}
