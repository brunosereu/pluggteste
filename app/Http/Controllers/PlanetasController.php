<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\CurlController;

class PlanetasController extends Controller
{
    //
    public function index(){
        $planetas = CurlController::get("planets/");
        // dd($planetas);
        return view('Painel.planetas',[
            'page'=>'planetas',
            'planetas'=>$planetas->results,
            'next'=>$planetas->next,
            'previous'=>$planetas->previous,
        ])->render();
    }
}
