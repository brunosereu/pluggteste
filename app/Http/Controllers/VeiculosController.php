<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\CurlController;

class VeiculosController extends Controller
{
    //
    public function index(){
        $veiculos = CurlController::get("vehicles/");
        // dd($veiculos);
        return view('Painel.veiculos',[
            'page'=>'veiculos',
            'veiculos'=>$veiculos->results,
            'next'=>$veiculos->next,
            'previous'=>$veiculos->previous,
        ])->render();
    }
}
