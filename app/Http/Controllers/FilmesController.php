<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\CurlController;

class FilmesController extends Controller
{
    //
    public function index(){

        $filmes = CurlController::get("films/");
        // dd($filmes);
        return view('Painel.filmes',[
            'page'=>'filmes',
            'filmes'=>$filmes->results,
            'next'=>$filmes->next,
            'previous'=>$filmes->previous,
        ])->render();
    }
}
