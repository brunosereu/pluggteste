<div class="sidebar" data-color="black" data-active-color="info">
    <div class="logo">
        <a href="{{ asset('/') }}" class="simple-text logo-mini">
            <div class="logo-image-small">
              <i class="fab fa-jedi-order" style="font-size: 30px;"></i>
            </div>
        </a>
        <a href="{{ asset('/') }}" class="simple-text logo-normal" style="max-width: 17ch;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;">Jedi System</a>
    </div>
    <div class="sidebar-wrapper">

        <ul class="nav">
            <li class="{{ $page == 'pessoas'  ? 'active' : ''}}">
                <a href="{{ asset('/pessoas') }}">
                  <i class="fas fa-user-astronaut"></i>
                  <span class="sidebar-normal">Pessoas</span>
                </a>
            </li>
            <li class="{{ $page == 'filmes'  ? 'active' : ''}}">
                <a href="{{ asset('/filmes') }}">
                  <i class="fas fa-journal-whills"></i>
                  <span class="sidebar-normal">Filmes</span>
                </a>
            </li>
            <li class="{{ $page == 'naves'  ? 'active' : ''}}">
                <a href="{{ asset('/naves') }}">
                <i class="fas fa-space-shuttle"></i>
                  <span class="sidebar-normal">Naves</span>
                </a>
            </li>
            <li class="{{ $page == 'veiculos'  ? 'active' : ''}}">
                <a href="{{ asset('/veiculos') }}">
                  <i class="fas fa-car"></i>
                  <span class="sidebar-normal">Veículos</span>
                </a>
            </li>
            <li class="{{ $page == 'especies'  ? 'active' : ''}}">
                <a href="{{ asset('/especies') }}">
                <i class="fas fa-hand-spock"></i>
                  <span class="sidebar-normal">Espécies</span>
                </a>
            </li>
            <li class="{{ $page == 'planetas'  ? 'active' : ''}}">
                <a href="{{ asset('/planetas') }}">
                  <i class="fas fa-globe"></i>
                  <span class="sidebar-normal">Planetas</span>
                </a>
            </li>
        </ul>
    </div>
</div>
