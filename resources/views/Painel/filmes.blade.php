@extends('Template.template')
@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title float-left">Lista de Filmes:</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="tableFilmes">
                            <thead class=" text-primary">
                                <th onclick="sortTable(0)" style="cursor:pointer;">
                                    Titulo do Filme
                                </th>
                                <th onclick="sortTable(1)" style="cursor:pointer;">
                                    Episodio
                                </th>
                                <th onclick="sortTable(2)" style="cursor:pointer;">
                                    Data de Lançamento
                                </th>
                            </thead>
                            <tbody id="Data">
                                @foreach($filmes as $filme)
                                <tr>
                                    <td>
                                    <a href="#" onclick="selectPerson('{{ $filme->url }}')">
                                        {{ $filme->title }}
                                    </a>
                                    </td>
                                    <td>
                                        {{ ($filme->episode_id) }} 
                                    </td>
                                    <td>
                                        {{ ( date('d/m/Y', strtotime($filme->release_date)) ) }}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="row" id="paginationButton">
                                @if($next == null && $previous != null )
                                   
                                    <div class="col-12">
                                        <button type="button" class="btn btn-primary float-left" onclick="selectPage('{{ $previous }}')">Anterior</button>
                                    </div>
                                    
                                @elseif($previous == null && $next != null)
                                    
                                    <div class="col-12">
                                        <button type="button" class="btn btn-primary float-right" onclick="selectPage('{{ $next }}')">Proximo</button>
                                    </div>
                                    
                                @elseif($previous != null && $next != null)
                                    
                                    <div class="col-6">
                                        <button type="button" class="btn btn-primary float-left" onclick="selectPage('{{ $previous }}')">Anterior</button>
                                    </div>
                                    <div class="col-6">
                                    <button type="button" class="btn btn-primary float-right" onclick="selectPage('{{ $next }}')">Proximo</button>
                                    </div>
                                

                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
<script src="js/filmes.js"></script>
<script>

</script>
@endsection