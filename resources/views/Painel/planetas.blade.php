@extends('Template.template')
@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title float-left">Lista de Planetas:</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <th>
                                    Nome do Planeta
                                </th>
                                <th>
                                    Horas de um Dia
                                </th>
                                <th>
                                    Dias para completar Orbita
                                </th>
                            </thead>
                            <tbody id="Data">
                                @foreach($planetas as $planeta)
                                <tr>
                                    <td>
                                    <a href="#" onclick="selectPerson('{{ $planeta->url }}')">
                                        {{ $planeta->name }}
                                    </a>
                                    </td>
                                    <td>
                                        {{ ( $planeta->rotation_period =='unknown'? 'Desconhecido': $planeta->rotation_period." Horas") }} 
                                    </td>
                                    <td>
                                        {{ ($planeta->orbital_period  =='unknown'? 'Desconhecido': $planeta->orbital_period ." Dias") }}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="row" id="paginationButton">
                                @if($next == null && $previous != null )
                                   
                                    <div class="col-12">
                                        <button type="button" class="btn btn-primary float-left" onclick="selectPage('{{ $previous }}')">Anterior</button>
                                    </div>
                                    
                                @elseif($previous == null && $next != null)
                                    
                                    <div class="col-12">
                                        <button type="button" class="btn btn-primary float-right" onclick="selectPage('{{ $next }}')">Proximo</button>
                                    </div>
                                    
                                @elseif($previous != null && $next != null)
                                    
                                    <div class="col-6">
                                        <button type="button" class="btn btn-primary float-left" onclick="selectPage('{{ $previous }}')">Anterior</button>
                                    </div>
                                    <div class="col-6">
                                    <button type="button" class="btn btn-primary float-right" onclick="selectPage('{{ $next }}')">Proximo</button>
                                    </div>
                                
                                 
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
<script src="js/planetas.js"></script>
<script>

</script>
@endsection