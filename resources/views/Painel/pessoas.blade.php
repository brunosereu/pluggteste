@extends('Template.template')
@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title float-left">Lista de Pessoas:</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <th>
                                    Nome
                                </th>
                                <th>
                                    Peso
                                </th>
                                <th>
                                    Genêro
                                </th>
                                <th>
                                    Altura
                                </th>
                            </thead>
                            <tbody id="peoppleData">
                                @foreach($pessoas as $pessoa)
                                <tr>
                                    <td>
                                    <a href="#" onclick="selectPerson('{{ $pessoa->url }}')">
                                        {{ $pessoa->name }}
                                    </a>
                                    </td>
                                    <td>
                                        {{ ($pessoa->mass == "unknown" ? 'Desconhecida' : $pessoa->mass.' Kg') }} 
                                    </td>
                                    <td>
                                        {{ ($pessoa->gender == 'male' ? 'Masculino': ($pessoa->gender == "female" ? 'Feminino' : 'N/A')) }}
                                    </td>
                                    <td >
                                        {{ ($pessoa->height == "unknown" ? 'Desconhecida' : $pessoa->height.' cm')  }} 
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="row" id="paginationButton">
                                @if($next == null && $previous != null )
                                   
                                    <div class="col-12">
                                        <button type="button" class="btn btn-primary float-left" onclick="selectPage('{{ $previous }}')">Anterior</button>
                                    </div>
                                    
                                @elseif($previous == null && $next != null)
                                    
                                    <div class="col-12">
                                        <button type="button" class="btn btn-primary float-right" onclick="selectPage('{{ $next }}')">Proximo</button>
                                    </div>
                                    
                                @elseif($previous != null && $next != null)
                                    
                                    <div class="col-6">
                                        <button type="button" class="btn btn-primary float-left" onclick="selectPage('{{ $previous }}')">Anterior</button>
                                    </div>
                                    <div class="col-6">
                                    <button type="button" class="btn btn-primary float-right" onclick="selectPage('{{ $next }}')">Proximo</button>
                                    </div>
                                
                                 
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
<script src="js/pessoas.js"></script>
@endsection