@extends('Template.template')
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('content')
<div class="content">
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-body ">
                <div class="row">
                    <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                        <i class="fas fa-user-astronaut text-info"></i>
                    </div>
                    </div>
                    <div class="col-7 col-md-8">
                    <div class="numbers">
                        <p class="card-category">Pessoas</p>
                        <p class="card-title">{{ $quantidadePessoas }} Registros
                        </p><p>
                    </p></div>
                    </div>
                </div>
                </div>
                <div class="card-footer ">
                <hr>
                <div class="stats">
                     <a href="{{ asset('/pessoas') }}"><button class="btn btn-default">Acessar Portal</button></a>
                </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-body ">
                <div class="row">
                    <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                        <i class="fas fa-journal-whills text-info"></i>
                    </div>
                    </div>
                    <div class="col-7 col-md-8">
                    <div class="numbers">
                        <p class="card-category">Filmes</p>
                        <p class="card-title">{{ $quantidadeFilmes }} Registros
                        </p><p>
                    </p></div>
                    </div>
                </div>
                </div>
                <div class="card-footer ">
                <hr>
                <div class="stats">
                     <a href="{{ asset('/filmes') }}"><button class="btn btn-default">Acessar Portal</button></a>
                </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-body ">
                <div class="row">
                    <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                        <i class="fas fa-space-shuttle text-info"></i>
                    </div>
                    </div>
                    <div class="col-7 col-md-8">
                    <div class="numbers">
                        <p class="card-category">Naves</p>
                        <p class="card-title">{{ $quantidadeNaves }}  Registros
                        </p><p>
                    </p></div>
                    </div>
                </div>
                </div>
                <div class="card-footer ">
                <hr>
                <div class="stats">
                     <a href="{{ asset('/naves') }}"><button class="btn btn-default">Acessar Portal</button></a>
                </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-body ">
                <div class="row">
                    <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                        <i class="fas fa-car text-info"></i>
                    </div>
                    </div>
                    <div class="col-7 col-md-8">
                    <div class="numbers">
                        <p class="card-category">Veículos</p>
                        <p class="card-title">{{ $quantidadeVeiculos }} Registros
                        </p><p>
                    </p></div>
                    </div>
                </div>
                </div>
                <div class="card-footer ">
                <hr>
                <div class="stats">
                     <a href="{{ asset('/veiculos') }}"><button class="btn btn-default">Acessar Portal</button></a>
                </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-body ">
                <div class="row">
                    <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                        <i class="fas fa-hand-spock text-info"></i>
                    </div>
                    </div>
                    <div class="col-7 col-md-8">
                    <div class="numbers">
                        <p class="card-category">Espécies</p>
                        <p class="card-title">{{ $quantidadeEspecies }} Registros
                        </p><p>
                    </p></div>
                    </div>
                </div>
                </div>
                <div class="card-footer ">
                <hr>
                <div class="stats">
                     <a href="{{ asset('/especies') }}"><button class="btn btn-default">Acessar Portal</button></a>
                </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-body ">
                <div class="row">
                    <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                        <i class="fas fa-globe text-info"></i>
                    </div>
                    </div>
                    <div class="col-7 col-md-8">
                    <div class="numbers">
                        <p class="card-category">Planetas</p>
                        <p class="card-title">{{ $quantidadePlanetas }} Registros
                        </p><p>
                    </p></div>
                    </div>
                </div>
                </div>
                <div class="card-footer ">
                <hr>
                <div class="stats">
                        <a href="{{ asset('/planetas') }}"><button class="btn btn-default">Acessar Portal</button></a>
                </div>
                </div>
            </div>
        </div>
    </div>
</div> 
@endsection


@section('js')
<script>
</script>
@endsection