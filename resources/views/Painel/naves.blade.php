@extends('Template.template')
@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title float-left">Lista de Naves:</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <th>
                                    Nome
                                </th>
                                <th>
                                    Modelo
                                </th>
                                <th>
                                    Quantidade de Passageiros
                                </th>
                            </thead>
                            <tbody id="Data">
                                @foreach($naves as $nave)
                                <tr>
                                    <td>
                                    <a href="#" onclick="selectPerson('{{ $nave->url }}')">
                                        {{ $nave->name }}
                                    </a>
                                    </td>
                                    <td>
                                        {{ ($nave->model) }} 
                                    </td>
                                    <td>
                                        {{ ($nave->passengers  =='unknown'? 'Desconhecido':$nave->passengers  ) }}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="row" id="paginationButton">
                                @if($next == null && $previous != null )
                                   
                                    <div class="col-12">
                                        <button type="button" class="btn btn-primary float-left" onclick="selectPage('{{ $previous }}')">Anterior</button>
                                    </div>
                                    
                                @elseif($previous == null && $next != null)
                                    
                                    <div class="col-12">
                                        <button type="button" class="btn btn-primary float-right" onclick="selectPage('{{ $next }}')">Proximo</button>
                                    </div>
                                    
                                @elseif($previous != null && $next != null)
                                    
                                    <div class="col-6">
                                        <button type="button" class="btn btn-primary float-left" onclick="selectPage('{{ $previous }}')">Anterior</button>
                                    </div>
                                    <div class="col-6">
                                    <button type="button" class="btn btn-primary float-right" onclick="selectPage('{{ $next }}')">Proximo</button>
                                    </div>
                                
                                 
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
<script src="js/naves.js"></script>
<script>

</script>
@endsection