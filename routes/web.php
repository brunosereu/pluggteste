<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
error_reporting(E_ERROR | E_PARSE);
Route::get('/',"PainelController@index");
Route::get('/pessoas',"PessoasController@index");
Route::get('/filmes',"FilmesController@index");
Route::get('/naves',"NavesController@index");
Route::get('/veiculos',"VeiculosController@index");
Route::get('/especies',"EspeciesController@index");
Route::get('/planetas',"PlanetasController@index");

