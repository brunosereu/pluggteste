function selectPerson(url){
    $.ajax({
        url: ""+url, 
        type:"GET",
        success: function(e){
            let j = Math.floor(Math.random() * 65536);
            $("body").append(`
            <div class="modal fade" data-backdrop="static" id="myModalNovo` + j + `" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title text-center">Informações sobre o Veículo : `+ e.name +`</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>
                                Modelo: <span>`+ e.model  +`</span>
                            </p>
                            <p>
                                Quantidade de Passageiros: <span>`+ (e.passengers == 'unknown' ? 'Desconhecido': e.passengers )+`</span>
                            </p>
                            <p>
                                Largura: <span>`+ (e.length  == "unknown" ? 'Desconhecida' : e.length +' M') +`</span>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>`);
            $("#myModalNovo" + j).on('hide.bs.modal', function () {
                $("#myModalNovo" + j).remove();
            });
            $("#myModalNovo" + j).modal('show');
        },
    });
}
function selectPage(url){
    $.ajax({
        url: ""+url, 
        type:"GET",
        success: function(e){
            $('#Data').html('');
            e.results.forEach(veiculo => {
                $('#Data').append(`
                <tr>
                    <td>
                    <a href="#" onclick="selectPerson('`+ veiculo.url +`')">
                        `+ veiculo.name +`
                    </a>
                    </td>
                    <td>
                        `+ veiculo.model +` 
                    </td>
                    <td>
                        `+ (veiculo.passengers =='unknown'? 'Desconhecido': veiculo.passengers )  +`
                    </td>
                </tr>
                `);
            });
            if(e.next == null && e.previous != null){
                $('#paginationButton').html(`
                <div class="col-12">
                    <button type="button" class="btn btn-primary float-left" onclick="selectPage('`+ e.previous +`')">Anterior</button>
                </div>
                `);
            }else if(e.previous == null && e.next != null){
                $('#paginationButton').html(`
                <div class="col-12">
                    <button type="button" class="btn btn-primary float-right" onclick="selectPage('`+ e.next +`')">Proximo</button>
                </div>
                `);
            }else if (e.previous != null && e.next != null){
                $('#paginationButton').html(`
                <div class="col-6">
                    <button type="button" class="btn btn-primary float-left" onclick="selectPage('`+ e.previous +`')">Anterior</button>
                </div>
                <div class="col-6">
                    <button type="button" class="btn btn-primary float-right" onclick="selectPage('`+ e.next +`')">Proximo</button>
                </div>
                `);
            }
            
        },
    });
}