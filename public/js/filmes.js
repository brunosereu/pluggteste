function selectPerson(url){
    $.ajax({
        url: ""+url, 
        type:"GET",
        success: function(e){
            let j = Math.floor(Math.random() * 65536);
            $("body").append(`
            <div class="modal fade" data-backdrop="static" id="myModalNovo` + j + `" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title text-center">Informações sobre o Episodio: `+ e.episode_id +`</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>
                                Titulo: <span>`+ e.title +`</span>
                            </p>
                            <p>
                                Data de Lançamento: <span>`+ formatDate(e.release_date) +`</span>
                            </p>
                            <p>
                                Diretor: <span>`+ e.director +`</span>
                            </p>
                            <p>
                                Produtor(es): <span>`+ e.producer +`</span>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>`);
            $("#myModalNovo" + j).on('hide.bs.modal', function () {
                $("#myModalNovo" + j).remove();
            });
            $("#myModalNovo" + j).modal('show');
        },
    });
}
function selectPage(url){
    $.ajax({
        url: ""+url, 
        type:"GET",
        success: function(e){
            $('#Data').html('');
            e.results.forEach(filme => {
                $('#Data').append(`
                <tr>
                    <td>
                    <a href="#" onclick="selectPerson('`+ filme.url +`')">
                        `+ filme.title +`
                    </a>
                    </td>
                    <td>
                        `+ filme.episode_id +` 
                    </td>
                    <td>
                        `+ formatDate(filme.release_date) +`
                    </td>
                </tr>
                `);
            });
            if(e.next == null && e.previous != null){
                $('#paginationButton').html(`
                <div class="col-12">
                    <button type="button" class="btn btn-primary float-left" onclick="selectPage('`+ e.previous +`')">Anterior</button>
                </div>
                `);
            }else if(e.previous == null && e.next != null){
                $('#paginationButton').html(`
                <div class="col-12">
                    <button type="button" class="btn btn-primary float-right" onclick="selectPage('`+ e.next +`')">Proximo</button>
                </div>
                `);
            }else if(e.previous != null && e.next != null){
                $('#paginationButton').html(`
                <div class="col-6">
                    <button type="button" class="btn btn-primary float-left" onclick="selectPage('`+ e.previous +`')">Anterior</button>
                </div>
                <div class="col-6">
                    <button type="button" class="btn btn-primary float-right" onclick="selectPage('`+ e.next +`')">Proximo</button>
                </div>
                `);
            }
            
        },
    });
}
function formatDate($d){
    var data = new Date(""+$d);

    var dia  = data.getDate();
    if (dia< 10) {
        dia  = "0" + dia;
    }

    var mes  = data.getMonth() + 1;
    if (mes < 10) {
        mes  = "0" + mes;
    }

    var ano  = data.getFullYear();
    dataFormatada = dia + "/" + mes + "/" + ano;
    return dataFormatada;
}
function sortTable(n) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("tableFilmes");
    switching = true;
    // Set the sorting direction to ascending:
    dir = "asc"; 
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
      // Start by saying: no switching is done:
      switching = false;
      rows = table.rows;
      /* Loop through all table rows (except the
      first, which contains table headers): */
      for (i = 1; i < (rows.length - 1); i++) {
        // Start by saying there should be no switching:
        shouldSwitch = false;
        /* Get the two elements you want to compare,
        one from current row and one from the next: */
        x = rows[i].getElementsByTagName("TD")[n];
        y = rows[i + 1].getElementsByTagName("TD")[n];
        /* Check if the two rows should switch place,
        based on the direction, asc or desc: */
        if (dir == "asc") {
          if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
            // If so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
        } else if (dir == "desc") {
          if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
            // If so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
        }
      }
      if (shouldSwitch) {
        /* If a switch has been marked, make the switch
        and mark that a switch has been done: */
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
        // Each time a switch is done, increase this count by 1:
        switchcount ++; 
      } else {
        /* If no switching has been done AND the direction is "asc",
        set the direction to "desc" and run the while loop again. */
        if (switchcount == 0 && dir == "asc") {
          dir = "desc";
          switching = true;
        }
      }
    }
  }
$(document).ready(function (){
    sortTable(1);
});