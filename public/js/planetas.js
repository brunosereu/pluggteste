function selectPerson(url){
    $.ajax({
        url: ""+url, 
        type:"GET",
        success: function(e){
            let j = Math.floor(Math.random() * 65536);
            $("body").append(`
            <div class="modal fade" data-backdrop="static" id="myModalNovo` + j + `" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title text-center">Informações sobre o Planeta : `+ e.name +`</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>
                                Horas de um Dia: <span>`+ (e.rotation_period =='unknown'? 'Desconhecido': e.rotation_period+ ' Horas' ) +` </span>
                            </p>
                            <p>
                                Dias para completar Orbita: <span>`+ (e.orbital_period =='unknown'? 'Desconhecido': e.orbital_period+ ' Dias' )  +`</span>
                            </p>
                            <p>
                                Diametro: <span>`+ (e.diameter  == "unknown" ? 'Desconhecida' : e.diameter ) +`</span>
                            </p>
                            <p>
                                População: <span>`+ (e.population  == "unknown" ? 'Desconhecida' : e.population + ' Pessoas' ) +`</span>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>`);
            $("#myModalNovo" + j).on('hide.bs.modal', function () {
                $("#myModalNovo" + j).remove();
            });
            $("#myModalNovo" + j).modal('show');
        },
    });
}
function selectPage(url){
    $.ajax({
        url: ""+url, 
        type:"GET",
        success: function(e){
            $('#Data').html('');
            e.results.forEach(planetas => {
                $('#Data').append(`
                <tr>
                    <td>
                    <a href="#" onclick="selectPerson('`+ planetas.url +`')">
                        `+ planetas.name +`
                    </a>
                    </td>
                    <td>
                        `+ (planetas.rotation_period =='unknown'? 'Desconhecido': planetas.rotation_period+ ' Horas' ) +` 
                    </td>
                    <td>
                        `+ (planetas.orbital_period =='unknown'? 'Desconhecido': planetas.orbital_period+ ' Dias' )  +`
                    </td>
                </tr>
                `);
            });
            if(e.next == null && e.previous != null){
                $('#paginationButton').html(`
                <div class="col-12">
                    <button type="button" class="btn btn-primary float-left" onclick="selectPage('`+ e.previous +`')">Anterior</button>
                </div>
                `);
            }else if(e.previous == null && e.next != null){
                $('#paginationButton').html(`
                <div class="col-12">
                    <button type="button" class="btn btn-primary float-right" onclick="selectPage('`+ e.next +`')">Proximo</button>
                </div>
                `);
            }else if (e.previous != null && e.next != null){
                $('#paginationButton').html(`
                <div class="col-6">
                    <button type="button" class="btn btn-primary float-left" onclick="selectPage('`+ e.previous +`')">Anterior</button>
                </div>
                <div class="col-6">
                    <button type="button" class="btn btn-primary float-right" onclick="selectPage('`+ e.next +`')">Proximo</button>
                </div>
                `);
            }
            
        },
    });
}