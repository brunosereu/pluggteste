function selectPerson(url){
    $.ajax({
        url: ""+url, 
        type:"GET",
        success: function(e){
            let j = Math.floor(Math.random() * 65536);
            $("body").append(`
            <div class="modal fade" data-backdrop="static" id="myModalNovo` + j + `" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title text-center">Informações sobre a Espécie : `+ e.name +`</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>
                                Tempo de vida médio: <span>`+ e.average_lifespan  +`</span>
                            </p>
                            <p>
                                Quantidade de Passageiros: <span>`+ (e.average_lifespan == 'unknown' ? 'Desconhecido': e.average_lifespan+ ' Anos' )+`</span>
                            </p>
                            <p>
                                Língua: <span>`+ (e.language  == "unknown" ? 'Desconhecida' : e.language ) +`</span>
                            </p>
                            <p>
                                Altura média: <span>`+ (e.average_height  == "unknown" ? 'Desconhecida' : e.average_height +' cm') +`</span>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>`);
            $("#myModalNovo" + j).on('hide.bs.modal', function () {
                $("#myModalNovo" + j).remove();
            });
            $("#myModalNovo" + j).modal('show');
        },
    });
}
function selectPage(url){
    $.ajax({
        url: ""+url, 
        type:"GET",
        success: function(e){
            $('#Data').html('');
            e.results.forEach(especies => {
                $('#Data').append(`
                <tr>
                    <td>
                    <a href="#" onclick="selectPerson('`+ especies.url +`')">
                        `+ especies.name +`
                    </a>
                    </td>
                    <td>
                        `+ (especies.average_lifespan =='unknown'? 'Desconhecido': especies.average_lifespan ) +` 
                    </td>
                    <td>
                        `+ (especies.language =='unknown'? 'Desconhecido': especies.language )  +`
                    </td>
                </tr>
                `);
            });
            if(e.next == null && e.previous != null){
                $('#paginationButton').html(`
                <div class="col-12">
                    <button type="button" class="btn btn-primary float-left" onclick="selectPage('`+ e.previous +`')">Anterior</button>
                </div>
                `);
            }else if(e.previous == null && e.next != null){
                $('#paginationButton').html(`
                <div class="col-12">
                    <button type="button" class="btn btn-primary float-right" onclick="selectPage('`+ e.next +`')">Proximo</button>
                </div>
                `);
            }else if (e.previous != null && e.next != null){
                $('#paginationButton').html(`
                <div class="col-6">
                    <button type="button" class="btn btn-primary float-left" onclick="selectPage('`+ e.previous +`')">Anterior</button>
                </div>
                <div class="col-6">
                    <button type="button" class="btn btn-primary float-right" onclick="selectPage('`+ e.next +`')">Proximo</button>
                </div>
                `);
            }
            
        },
    });
}