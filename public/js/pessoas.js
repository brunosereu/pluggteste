function selectPerson(url){
    $.ajax({
        url: ""+url, 
        type:"GET",
        success: function(e){
            let j = Math.floor(Math.random() * 65536);
            $("body").append(`
            <div class="modal fade" data-backdrop="static" id="myModalNovo` + j + `" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title text-center">Informações sobre : `+ e.name +`</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>
                                Peso: <span>`+ (e.mass == "unknown" ? 'Desconhecida' : e.mass + ' Kg')  +`</span>
                            </p>
                            <p>
                                Genêro: <span>`+ (e.gender == 'male' ? 'Masculino': (e.gender == "female" ? 'Feminino' : 'N/A')) +`</span>
                            </p>
                            <p>
                                Altura: <span>`+ (e.height == "unknown" ? 'Desconhecida' : e.height+' cm') +`</span>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>`);
            $("#myModalNovo" + j).on('hide.bs.modal', function () {
                $("#myModalNovo" + j).remove();
            });
            $("#myModalNovo" + j).modal('show');
        },
    });
}
function selectPage(url){
    $.ajax({
        url: ""+url, 
        type:"GET",
        success: function(e){
            $('#peoppleData').html('');
            e.results.forEach(pessoas => {
                $('#peoppleData').append(`
                <tr>
                    <td>
                    <a href="#" onclick="selectPerson('`+ pessoas.url +`')">
                        `+ pessoas.name +`
                    </a>
                    </td>
                    <td>
                        `+ (pessoas.mass == "unknown" ? 'Desconhecida' : pessoas.mass + ' Kg') +` 
                    </td>
                    <td>
                        `+ (pessoas.gender == 'male' ? 'Masculino': (pessoas.gender == "female" ? 'Feminino' : 'N/A')) +`
                    </td>
                    <td >
                        `+ (pessoas.height == "unknown" ? 'Desconhecida' : pessoas.height+' cm') +` 
                    </td>
                </tr>
                `);
            });
            if(e.next == null && e.previous != null){
                $('#paginationButton').html(`
                <div class="col-12">
                    <button type="button" class="btn btn-primary float-left" onclick="selectPage('`+ e.previous +`')">Anterior</button>
                </div>
                `);
            }else if(e.previous == null && e.next != null){
                $('#paginationButton').html(`
                <div class="col-12">
                    <button type="button" class="btn btn-primary float-right" onclick="selectPage('`+ e.next +`')">Proximo</button>
                </div>
                `);
            }else if (e.previous != null && e.next != null){
                $('#paginationButton').html(`
                <div class="col-6">
                    <button type="button" class="btn btn-primary float-left" onclick="selectPage('`+ e.previous +`')">Anterior</button>
                </div>
                <div class="col-6">
                    <button type="button" class="btn btn-primary float-right" onclick="selectPage('`+ e.next +`')">Proximo</button>
                </div>
                `);
            }
            
        },
    });
}